import React from 'react';
import { Redirect } from 'react-router-dom';
import * as API_COMMENTS from "../log/api/comment-api";
import * as API_PLACES from "../admin/api/place-api";
import Button from "react-bootstrap/Button";
import {Col, Form, Row} from "react-bootstrap";
import * as API_USERS from "../admin/api/place-api";
import * as API_WISHLIST from "../wishlist/api/wishlist-api";

class DetailsPageUser extends React.Component {

    constructor(props) {
        super(props);
        this.id_place1 = "";
        this.handleBack = this.handleBack.bind(this);
        this.state = {
            back: false,
            items: [],
            isLoaded: false,
            ///
            description:'',
            id_place:59,
            user_email:null,
            grade:null,
            data:[]
        };


    }
    openFacebook() {
        const url = 'https://www.facebook.com/';
        window.open(url, '_blank');
    }
    openInstagram() {
        const url = 'https://www.instagram.com/';
        window.open(url, '_blank');
    }
    openTwitter() {
        const url = 'https://twitter.com/?lang=ro';
        window.open(url, '_blank');
    }

    /////
    handleAddComment = event => {
        const id=localStorage.getItem("id_place");
        const email=sessionStorage.getItem("Email");
        console.log("email "+email);
        let commentdata = {
            description: this.state.description,
            id_place:id,
            grade:this.state.grade,
            user_email:email

        };
        event.preventDefault();

        API_COMMENTS.insertComment(id,commentdata)

        window.location.reload(false);

    };


 /*   handleViewComment = () => {
        API_COMMENTS.getComments((result, status, err)
            .then(response => this.setState({data:response.data}))
            .catch(e => console.log(e));
    };*/


    handleViewComment() {
        const id=localStorage.getItem("id_place");
        console.log(id);
        return API_COMMENTS.getComments(id, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    data: result,

                });
                console.log(id+" "+result);
            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });

    }


    componentDidMount() {
        this.fetchPlace(this.id_place1);
        /////
        this.handleViewComment();
    }

    fetchPlace(id_place1) {
        return API_PLACES.getPlaceById(id_place1, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    items: result,
                    isLoaded: true
                });
            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    handleBack() {
        this.setState(({
            back: true
        }))
    }
    render() {


        const { back } = this.state;



        if (back) {
            return (<Redirect to={{
                pathname: '/user'
            }} />);
        }

        return (
            <div>
                <header className="header-home">
                    <section className="pure-form-section">
                        <form className="pure-form pure-form-mine">
                            <button type="button" id="log-out" className="pure-button pure-button-primary home-buttons" onClick={this.handleBack}>Back</button>
                        </form>
                    </section>
                </header>
                <main className="main-home2">

                    <div>
                        <br/><br/><br/><br/>
                        <div className="column4">
                            <img src={sessionStorage.getItem('Image')} width="260" height="380">
                            </img>
                        </div>
                        <div className="column5">
                            <h5>Name: {sessionStorage.getItem('Name')}</h5>
                            <h5>Location: {sessionStorage.getItem('Location')}</h5>
                            <h5>Description: {sessionStorage.getItem('DescriptionText')}</h5>
                            <h5>Price: {sessionStorage.getItem('Price')}</h5>
                            <h5>Category: {sessionStorage.getItem('Category')}</h5>
                            <div className="column4">
                                <iframe width="360" height="200" src={sessionStorage.getItem('Audio')}
                                        title="YouTube video player" frameBorder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowFullScreen></iframe>
                            </div>

                        </div>
                    </div>

                </main>
                <br/>
                <Form onSubmit={this.handleAddComment}>
                    <Form.Group as={Row} controlId="formHorizontalDescription">
                        <div className="columnReview">
                            <textarea cols={80} rows={4} type="text" placeholder="Write a comment.." value={this.state.description} onChange={e => this.setState({description:e.target.value})} />




                                <textarea cols={20} rows={2} type="text" placeholder="Write a grade.." value={this.state.grade} onChange={e => this.setState({grade:e.target.value})} />
                             </div>

                        <Col>

                            <br/>  <br/> <br/>  <br/>  <br/>  <br/>  <br/> <Button  color="primary"  type="submit" >Add review</Button>


                        </Col>
                    </Form.Group>
                </Form>
                <div className="columnReview">
                <div className="row bootstrap snippets">
                    <div className="col-md-6 col-md-offset-2 col-sm-12">
                        <div className="comment-wrapper">
                            <div className="panel panel-info">
                                <h2 className="panel-heading" style={{color: 'black'}}>Comments: </h2>
                                <div className="panel-body media-list clearfix pull-left ">
                                    {this.state.data.map(comment => {return(<div>
                                            <p> {comment.user_email}</p> <p className="text-success">>> {comment.description}</p>
                                            <p className="text-success">>> {comment.grade}</p>
                                            <hr className="hr" /><br/></div>
                                    )})}
                                </div></div></div></div></div></div>



            </div>
        )
    }

}
export default DetailsPageUser;

