import React from 'react';
import styles from './../home/home.css';
import * as CREATE_ACCOUNT from "./api/CreateAccount-api"
import { Redirect } from 'react-router-dom';
import validate from "../validators/validator";
import {
    Button,
    Input
} from 'reactstrap';
import validator from "../validators/validator";

class CreateAccountPage extends React.Component {

    constructor(props) {
        super(props);
        
        this.email = "";
        this.username = "";
        this.password = "";


        this.state = {
            redirectCreate: false,
            email: ' ',
            username: '',
            password: '',
            goOut: false,

        };

        this.handleCreateAccount = this.handleCreateAccount.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleLogOut = this.handleLogOut.bind(this);
        this.registerAccount = this.registerAccount.bind(this);
        this.validateForm=this.validateForm.bind(this);
    }
    openFacebook() {
        const url = 'https://www.facebook.com/';
        window.open(url, '_blank');
    }
    openInstagram() {
        const url = 'https://www.instagram.com/';
        window.open(url, '_blank');
    }
    openTwitter() {
        const url = 'https://twitter.com/?lang=ro';
        window.open(url, '_blank');
    }


    handleCreateAccount() {
        let create = {
            email: this.email,
            username: this.username,
            password: this.password
        };
        console.log("email is: " + this.username);
        console.log("password is: " + create.password);
    }

    validateForm()
    {
        const emailField=this.email;
        const passwordField=this.password;
        const usernameField=this.username;
       // if(emailField.length<3)
        const formatEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const formatPass=/^[A-Za-z0-9]\w{8,}$/;


        const ok1= formatEmail.test(emailField.toLowerCase());
        const ok2=formatPass.test(passwordField.toLowerCase());
        if(emailField=="" || passwordField=="" || usernameField =="")
        {
            alert("Make sure that you complete all the fields!!");
            return false;
        }
        else if(ok1==true && ok2==false)
        {alert("Make sure that your password has more than 8 chars!!");
            return false;}
        else if(ok1==false && ok2==true)
        {
            alert("Make sure that your email has a correct format!!");
                return false;
        }
        else if(ok1==false && ok2==false)
        {
            alert("Make sure that your email has a correct format and your password has more than 8 chars!!");
            return false;
        }


        else return true;

    }

    registerAccount(account) {
        return CREATE_ACCOUNT.insertAccount(account, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted account with id: " + result);
                window.alert("Successfully created!");


            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {

        this.setState(({
            redirectCreate: true
        }));

        let account = {

            email: this.email,
            username: this.username,
            password: this.password,

            
        };
        console.log(account);


     if(   this.validateForm()==true){
        this.registerAccount(account);}

    }
    handleLogOut() {
        this.setState(() => ({ goOut: true }))
    }
    render() {

        const { redirectCreate } = this.state;
        if (redirectCreate === true) {
            return (<Redirect to={{
                pathname: '/'
            }} />);
        }
        const { goOut } = this.state;

        if (goOut) {
            return (<Redirect to={{
                pathname: '/'
            }} />);
        }
        return (
            <div>
                <header className="header-home">
                    <section className="pure-form-section">
                        <form className="pure-form pure-form-mine">
                            <button type="button" id="log-out" className="pure-button pure-button-primary home-buttons" onClick={this.handleLogOut}>Back</button>
                        </form>
                    </section>
                </header>

                <main className="main-home"></main>
                    <div className="CreateAccount">

                        <div className="Account">

                            <div className="Register">

                                <h1>Register Page</h1>

                            </div>



                            <div className="email">
                                <label>Email</label>
                                <input name='email' id='emailField' placeholder={"Set your email"}
                                       onChange={(e) => {
                                           this.email = (e.target.value);

                                       }}
                                       type="text"
                                />
                            </div>


                            <div className="username">
                                <label>Username</label>
                                <input name='username' id='usernameField' placeholder={"Set your username"}
                                    onChange={(e) => {
                                        this.username = (e.target.value);

                                    }}

                                    type="text"
                                />

                            </div>

                            <div className="password">
                                <label>Password</label>
                                <input name='password' id='passwordField' placeholder={"Set your password"}
                                    onChange={(e) => {
                                        this.password = (e.target.value);
                                    }}
                                    type="password"
                                />
                            </div>

                            <div className="buttonCreate">
                                <button className="btnCrt" block type={"button"} onClick={this.handleSubmit}>
                                    Sign Up
                        </button>
                            </div>
                        </div>


                    </div>

                <footer >

                    <div className="icons">
                        <ul className="icons-ul">
                            <li><button id="facebook" onClick={this.openFacebook}></button></li>
                            <li><button id="instagram" onClick={this.openInstagram}></button></li>
                            <li><button id="twitter" onClick={this.openTwitter}></button></li>
                        </ul>
                    </div>

                </footer>
            </div>
        )
    }

}
export default CreateAccountPage;

