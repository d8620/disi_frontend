import {HOST} from '../../commons/host';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    log: '/person/register',
};


function insertAccount(user, callback){
    let request = new Request(HOST.backend_api + endpoint.log , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function getAccountById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.log +"/get-by-id/"+params, {
        method: 'GET'
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}
function deleteAccount(params, callback){
    let request = new Request(HOST.backend_api + endpoint.log+"/"+params, {
        method: 'DELETE',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}
function updateAccount(user, callback){
    let request = new Request(HOST.backend_api + endpoint.log , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}
export {
    insertAccount,
    deleteAccount,
    getAccountById,
    updateAccount
};