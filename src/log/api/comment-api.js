import {HOST} from '../../commons/host';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    comment: '/details-user',
};

function getComments(placeId, callback) {
    let request = new Request(HOST.backend_api + endpoint.comment+"/comments/"+placeId ,{
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCommentById(commentId, callback){
    let request = new Request(HOST.backend_api + endpoint.comment + "/" + commentId, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function insertComment(placeId,params, callback){
    let request = new Request(HOST.backend_api + endpoint.comment+"/reviews/" +placeId, {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(params)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getComments,
    getCommentById,
    insertComment,
};