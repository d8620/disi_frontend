import {HOST} from "./../../commons/host";
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    account: '/person'
};

function resetPassAccount(user, callback) {
    // console.log(user);
    let request = new Request(HOST.backend_api + endpoint.account + "/reset-password", {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)

    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    resetPassAccount,

}