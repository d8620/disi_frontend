import React from 'react';
import { Redirect } from 'react-router-dom';

import * as API_PLACES from "../admin/api/place-api";

class DetailsPage extends React.Component {

    constructor(props) {
        super(props);
        this.id = "";
        this.handleBack = this.handleBack.bind(this);
        this.state = {
            back: false,
            items: [],
            isLoaded: false,
        };


    }
    openFacebook() {
        const url = 'https://www.facebook.com/';
        window.open(url, '_blank');
    }
    openInstagram() {
        const url = 'https://www.instagram.com/';
        window.open(url, '_blank');
    }
    openTwitter() {
        const url = 'https://twitter.com/?lang=ro';
        window.open(url, '_blank');
    }

    componentDidMount() {
        this.fetchPlace(this.id);
    }

    fetchPlace(id) {
        return API_PLACES.getPlaceById(id, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    items: result,
                    isLoaded: true
                });
            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    handleBack() {
        this.setState(({
            back: true
        }))
    }
    render() {


        const { back } = this.state;

        if (back) {
            return (<Redirect to={{
                pathname: '/'
            }} />);
        }
        return (
            <div>
                <header className="header-home">
                    <section className="pure-form-section">
                        <form className="pure-form pure-form-mine">
                            <button type="button" id="log-out" className="pure-button pure-button-primary home-buttons" onClick={this.handleBack}>Back</button>
                        </form>
                    </section>
                </header>
                <main className="main-home2">

                    <div>
                        <br/><br/><br/><br/>
                        <div className="column4">
                            <img src={sessionStorage.getItem('Image')} width="260" height="380">
                            </img>
                        </div>
                        <div className="column5">
                            <h5>Name: {sessionStorage.getItem('Name')}</h5>
                            <h5>Location: {sessionStorage.getItem('Location')}</h5>
                            <h5>Description: {sessionStorage.getItem('DescriptionText')}</h5>
                            <h5>Price: {sessionStorage.getItem('Price')}</h5>
                            <h5>Category: {sessionStorage.getItem('Category')}</h5>
                            <div className="column4">
                                <iframe width="360" height="200" src={sessionStorage.getItem('Audio')}
                                        title="YouTube video player" frameBorder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowFullScreen></iframe>
                            </div>
                        </div>
                    </div>

                </main>

                <footer >
                    <center>
                        <br/>
                        <div className="icons">
                            <ul className="icons-ul">
                                <li><button id="facebook" onClick={this.openFacebook}></button></li>
                                <li><button id="instagram" onClick={this.openInstagram}></button></li>
                                <li><button id="twitter" onClick={this.openTwitter}></button></li>
                            </ul>
                        </div>
                    </center>
                </footer>
            </div>
        )
    }

}
export default DetailsPage;

