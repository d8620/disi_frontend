import React from 'react';
import styles from './../home/home.css';
import * as API_RESET_PASS from "../log/api/reset-pass-api"
import { Redirect } from 'react-router-dom';
class ResetPass extends React.Component {

    constructor(props) {
        super(props);
        this.email = '';
        this.newPassword = '';
        this.oldPassword = '';
        this.handleReset = this.handleReset.bind(this);
        this.handleBack = this.handleBack.bind(this);

        this.account = {
            id: null,
            email: '',
            password: '',
            role: ''
        };
        this.state = {
            reseted: false,
            back: false,
            errorStatus: 0,
            error: null,
            email: '',
            password: '',
            role: '',
            oldPassword: '',
            account: {
                id: null,
                email: '',
                password: '',
                role: ''
            },


        }
    }
    openFacebook() {
        const url = 'https://www.facebook.com/';
        window.open(url, '_blank');
    }
    openInstagram() {
        const url = 'https://www.instagram.com/';
        window.open(url, '_blank');
    }
    openTwitter() {
        const url = 'https://twitter.com/?lang=ro';
        window.open(url, '_blank');
    }


    handleReset() {
        
        let resetForm={
            email: this.email,
            oldPassword: this.oldPassword,
            newPassword: this.newPassword,
        };

        this.resetPassAccount(resetForm);
    }

    handleBack() {
        this.setState(({
            back: true
        }))
    }




    resetPassAccount(resetForm) {


        console.log("--------", resetForm);
        return API_RESET_PASS.resetPassAccount(resetForm, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully reseted with id: " + result);
                window.alert("Successfully reseted!")
                this.setState((
                    {
                        reseted: true
                    }
                ));
            } else {
                window.alert("Problem occured! Check user's email");
                this.setState((
                    {
                        errorStatus: status,
                        error: error
                    }
                ));
            }
        });


    }
    render() {

        const { reseted, back } = this.state;
        if (reseted === true) {
            return (<Redirect to={{
                pathname: '/'
            }} />);
        }
        if (back === true) {
            return (<Redirect to={{
                pathname: '/'
            }} />);
        }

        return (
            <div>
                <header className="header-home">
                    <section className="pure-form-section">
                        <form className="pure-form pure-form-mine">
                            <button type="button" id="log-out" className="pure-button pure-button-primary home-buttons" onClick={this.handleBack}>Back</button>
                        </form>
                    </section>
                </header>
                <main className="main-home">

                </main>
                <section className="pure-form-section">
                    <form className="pure-form" >
                        <fieldset className="fields-form my-reset-form">

                            <div className="reset-background">
                                <div className="ResetPassword">


                                <h1> Reset your password</h1>
                                    <br/>
                                    <br/>
                                    <br/>

                                </div>

                                <div className="email">
                                    <label>Email </label>
                                    <input name='email' id='email' placeholder={"Type your email"}
                                           onChange={(e) => {
                                               this.email = (e.target.value);
                                           }}
                                           type="email"
                                    />
                                </div>

                               <br/>
                                <br/>



                                <div className="email">
                                    <label>Old Password </label>
                                    <input name='oldPassword' id='oldPassword' placeholder={"Type your old password"}
                                           onChange={(e) => {
                                               this.oldPassword = (e.target.value);
                                           }}
                                           type="password"
                                    />
                                </div>

                                <br/>
                                <br/>


                                <div className="email">
                                    <label>New Password </label>
                                    <input name='newPassword' id='newPassword' placeholder={"Type your new password"}
                                           onChange={(e) => {
                                               this.newPassword = (e.target.value);
                                           }}
                                           type="password"
                                    />
                                </div>

                                <div className="buttonCreate2" >
                                    <button className="btnCrt" block type={"button"} onClick={this.handleReset}>
                                        Reset
                                    </button>
                                </div>

                            </div>



                        </fieldset>
                    </form>
                </section>

                <footer >

                    <div className="icons">
                        <ul className="icons-ul">
                            <li><button id="facebook" onClick={this.openFacebook}></button></li>
                            <li><button id="instagram" onClick={this.openInstagram}></button></li>
                            <li><button id="twitter" onClick={this.openTwitter}></button></li>
                        </ul>
                    </div>

                </footer>
            </div>
        );
    }
}

export default ResetPass