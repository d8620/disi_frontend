import {HOST} from '../../commons/host';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    comment: '/user',
};


function insertIntoWishlist(params, callback){
    let request = new Request(HOST.backend_api + endpoint.comment, {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(params)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    insertIntoWishlist


};