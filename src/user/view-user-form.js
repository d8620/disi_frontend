import React from 'react';

import { Redirect } from 'react-router-dom';
import * as API_PLACES from "../admin/api/place-api";
import * as API_SEARCH from "../search/api/search-api";
import * as API_WISHLIST from "../user/api/user-api";
import {Col} from "react-bootstrap";
import * as API_USERS from "../admin/api/place-api";
import * as SockJS from "sockjs-client";
import * as Stomp from "stompjs";
import {HOST} from "../commons/host";


const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
};
const headerStyle = { backgroundColor: 'white', height: "700px", width: "100%" };

class UserView extends React.Component {


    constructor(props) {
        super(props);
        this.locationFilled="";
        this.categoryFilled="";
        this.exist = true;
        this.state = {
            searchByLocation:false,
            searchByCategory:false,
            placeLocation:"",
            placeCategory:"",
            items: [],
            isLoaded: false,
            sendEmail: false,
            id_place: null,
            id_user: null,
            redirectWish:false,
            viewWishlist:false

        };


        this.openFacebook = this.openFacebook.bind(this);
        this.openTwitter = this.openTwitter.bind(this);
        this.openInstagram = this.openInstagram.bind(this);
        this.handleLogOut = this.handleLogOut.bind(this);
        this.handleSearchByLocation = this.handleSearchByLocation.bind(this);
        this.handleSearchByCategory = this.handleSearchByCategory.bind(this);
        this.handleViewDetails = this.handleViewDetails.bind(this);
        this.handleSendEmail = this.handleSendEmail.bind(this);
        this.handleWishlist = this.handleWishlist.bind(this);
        this.handleViewWishlist = this.handleViewWishlist.bind(this);
    }

    openFacebook() {
        const url = 'https://www.facebook.com/';
        window.open(url, '_blank');
    }
    openInstagram() {
        const url = 'https://www.instagram.com/';
        window.open(url, '_blank');
    }
    openTwitter() {
        const url = 'https://twitter.com/?lang=ro';
        window.open(url, '_blank');
    }


    handleLogOut() {

        this.setState(({
            redirectLogout: true
        }));
    }
    handleSendEmail() {

        this.setState(({
            sendEmail: true
        }));
    }
    handleViewWishlist() {

        this.setState(({
            viewWishlist: true
        }));
    }

    handleWishlist() {
        let wish = {
            id_place: localStorage.getItem("id_place"),
            id_user: localStorage.getItem("userId")

        };
        console.log(wish);
        this.props.action === 'update' ? this.updatePlace(wish, this.props.id) : this.insertIntoWishlist(wish)
        this.setState(({
            redirectWish: true
        }));
    }

    updatePlace(place, id) {
        return API_USERS.putPlace(id, place, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated place with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }
    handleViewDetails() {
        this.setState({
            viewDetails: true
        })
    }

    componentDidMount() {
        this.fetchPlaces()
        // this.fetchPlace();
        this.connectToWebsocket();

    }

    connectToWebsocket() {
        const websoket = new SockJS(HOST.backend_api + '/socket')
        const stompClient = Stomp.over(websoket)
        stompClient.connect({},()=>{
            stompClient.subscribe('/topic/socket/place/values', not=>{
                alert(not.body);
            })
        })
    }

    fetchPlaces() {

        return API_PLACES.getPlaces((result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    items: result,
                    isLoaded: true
                });
            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    handleSearchByLocation() {
        const placeLocation=this.state.placeLocation;
        console.log(placeLocation);

        return API_SEARCH.getPlacesByName(placeLocation,(result, status, err) => {
            if (result !== null && status === 200) {
                console.log(result);
                this.setState({
                    items: result,
                    isLoaded: true,
                    searchByLocation: true

                });
            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


    handleSearchByCategory() {
        const placeCategory=this.state.placeCategory;
        console.log(placeCategory);

        return API_SEARCH.getPlacesByCategory(placeCategory,(result, status, err) => {
            if (result !== null && status === 200) {
                console.log(result);
                this.setState({
                    items: result,
                    isLoaded: true,
                    searchByCategory: true

                });
            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    insertIntoWishlist(wish) {
        return API_WISHLIST.insertIntoWishlist( wish,(result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted in wishlist: " + result);
            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    render() {
        if (localStorage.getItem('ROLE') === 'user') {

            const {isLoaded, items} = this.state;
            const {redirectLogout} = this.state;
            const {viewDetails} = this.state;
            const {sendEmail} = this.state;
            const {viewWishlist} = this.state;
            const {redirectWish} = this.state;
            if (redirectLogout === true) {

                return (<Redirect to={{
                    pathname: '/'
                }}/>);
            }

            if (viewDetails === true) {
                return (<Redirect to={{
                    pathname: '/details-user'
                }}/>);
            }

            if (redirectWish === true) {
                return (<Redirect to={{
                    pathname: '/wishlist'
                }}/>);
            }

            if (sendEmail === true) {

                return (<Redirect to={{
                    pathname: '/contact'
                }}/>);
            }

            if (viewWishlist === true) {

                return (<Redirect to={{
                    pathname: '/wishlist'
                }}/>);
            }

            return (
                <div>
                    <header className="header-home">
                        <section className="pure-form-section">
                            <form className="pure-form pure-form-mine">
                                <button type="button" id="log-out"
                                        className="pure-button pure-button-primary home-buttons"
                                        onClick={this.handleLogOut}>Logout
                                </button>
                            </form>
                        </section>
                    </header>
                    <fieldset className="search-field-name">
                        <input type="place location" placeholder="Place location"
                               onChange={(e) => {
                                   this.state.placeLocation = (e.target.value);
                                   this.locationFilled = (e.target.value);
                               }}
                        />
                    </fieldset>
                    <button type="button" className="search-button-name" onClick={this.handleSearchByLocation}>Search by
                        location
                    </button>

                    <fieldset className="search-field-category">
                        <input type="place category" placeholder="Place category"
                               onChange={(e) => {
                                   this.state.placeCategory = (e.target.value);
                                   this.categoryFilled = (e.target.value);

                               }}

                        />
                    </fieldset>
                    <button type="button" className="search-button-category"
                            onClick={this.handleSearchByCategory}>Search by category
                    </button>
                    <button type="button" className="send_email"
                            onClick={this.handleSendEmail}>Send email
                    </button>
                    <button type="button" className="view_wishlist"
                            onClick={this.handleViewWishlist}>View Wishlist
                    </button>


                    <main className="user-home">

                        <ul>
                            <br/><br/><br/><br/><br/><br/> <br/> <br/>
                            {items.map(item => (
                                <li key={item.id} className="box">
                                    {localStorage.setItem('id_place', item.id)}
                                    <div className="column">
                                        <img src={item.image} width="100" height="160">
                                        </img>
                                        {sessionStorage.setItem('Image', item.image)}
                                        <button type="button" className="buttonDetails"
                                                onClick={this.handleViewDetails}>View details
                                        </button>
                                        <div className="columnWish">
                                            <button type="button" className="buttonWishlist"
                                                    onClick={this.handleWishlist}>Add to wishlist
                                            </button>
                                        </div>
                                    </div>
                                    <strong> Name: </strong> {item.name} {sessionStorage.setItem('Name', item.name)}
                                    <br/>
                                    <strong> Location: </strong> {item.location} {sessionStorage.setItem('Location', item.location)}
                                    <br/>
                                    <strong> Description: </strong> {item.descriptionText} {sessionStorage.setItem('DescriptionText', item.descriptionText)}
                                    <br/>
                                    <strong> Price: </strong> {item.price} € {sessionStorage.setItem('Price', item.price)}
                                    <br/>
                                    <strong> Category: </strong> {item.category} {sessionStorage.setItem('Category', item.category)}

                                </li>
                            ))}
                        </ul>


                    </main>
                    <footer>
                        <center>
                            <br/>
                            <div className="icons">
                                <ul className="icons-ul">
                                    <li>
                                        <button id="facebook" onClick={this.openFacebook}></button>
                                    </li>
                                    <li>
                                        <button id="instagram" onClick={this.openInstagram}></button>
                                    </li>
                                    <li>
                                        <button id="twitter" onClick={this.openTwitter}></button>
                                    </li>
                                </ul>
                            </div>
                        </center>

                    </footer>
                </div>
            );
        } else {
            return (
                <Col sm={{offset: 1}}>
                    <br/>
                    <h1> You must login as an Tourist! </h1>
                </Col>
            )
        }
    }
}

export default UserView
