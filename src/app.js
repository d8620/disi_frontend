import './app.css';
import Home from './home/home';
import ResetPass from './log/reset-pass'

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import React from 'react'
import CreateAccountPage from './log/CreateAccount-page'


import PlacesCRUD from "./admin/place_container";
import AdminPage from "./admin/place_container";
import UserView from "./user/view-user-form";
import DetailsPage from "./log/Details-page";
import DetailsPageUser from "./log/Details-page-user";
import View from "./chart/View";
import SendEmail from "./email/SendEmail";
import Wishlist from "./wishlist/Wishlist";
import ViewLocations from "./chart/ViewLocations";








class App extends React.Component {
    render() {
        return (
            <div>
                <Router>
                    <div>
                        <Switch>
                            <Route
                                exact
                                path='/'
                                render={() => <Home />}
                            />


                            <Route
                                exact
                                path='/reset-password'
                                render={() => <ResetPass />}
                            />

                            <Route
                                exact
                                path='/create-account'
                                render={() => <CreateAccountPage />}
                            />

                            <Route
                                exact
                                path='/admin'
                                render={() => <AdminPage />}
                            />

                            <Route
                                exact
                                path='/user'
                                render={() => <UserView />}
                            />


                            <Route
                                exact
                                path='/details'
                                render={() => <DetailsPage />}
                            />

                            <Route
                                exact
                                path='/details-user'
                                render={() => <DetailsPageUser />}
                            />

                            <Route
                                exact
                                path='/objectivesGraphic'
                                render={() => <View />}
                            />

                            <Route
                                exact
                                path='/locationsGraphic'
                                render={() => <ViewLocations />}
                            />

                            <Route
                                exact
                                path='/contact'
                                render={() => <SendEmail />}
                            />

                            <Route
                                exact
                                path='/wishlist'
                                render={() => <Wishlist />}
                            />



                            {/*Error*/}

                        </Switch>
                    </div>
                </Router>
            </div>
        )
    };
}

export default App;
