import {HOST} from '../../commons/host';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    place: '/place',
};

function getObjectivesGraphic(callback) {
    let request = new Request(HOST.backend_api + endpoint.place+"/objectivesGraphic" ,{
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}




export {
    getObjectivesGraphic
};