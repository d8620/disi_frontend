import React, { Component } from 'react';
import './chart.css';
import {Bar} from "react-chartjs-2";
import * as API_USERS from "../admin/api/place-api"

import { Chart as ChartJS } from 'chart.js/auto'
import { Chart }            from 'react-chartjs-2'
import {Col} from "react-bootstrap";
import {Redirect} from "react-router-dom";


class View extends Component {
    constructor(){
        super();
        this.handleBack = this.handleBack.bind(this);
        this.state = {
            chartData:{},
            back: false,
            nr_tourist:[],
            locations:[]
        }
    }

    static defaultProps = {
        displayTitle:true,
        displayLegend: true,
        legendPosition:'right',
        location:'City'
    }
    handleBack() {
        this.setState(({
            back: true
        }))
    }

    componentWillMount(){
        // this.getchartData(); // this should be this.getChartData();
        this.getChartData();
    }
    componentDidMount() {
        this.fetchPlaces();
    }

    fetchPlaces() {
        return API_USERS.getPlaces((result, status, err) => {
            if (result !== null && status === 200) {
                for (const i in result) {
                    this.state.nr_tourist.push(result[i]['nr_of_tourists']);
                    this.state.locations.push(result[i]['location']);

                }
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
                console.log(this.state.nr_tourist);
                console.log(this.state.locations);

            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });


    }

    getChartData(){
        // Ajax calls here
        this.setState({
            chartData:{
                labels: this.state.locations,
                datasets:[
                    {
                        label:'Population',
                        data:this.state.nr_tourist,
                        backgroundColor:[
                            'rgba(255, 99, 132, 0.6)',
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(255, 206, 86, 0.6)',
                            'rgba(75, 192, 192, 0.6)',
                            'rgba(153, 102, 255, 0.6)',
                            'rgba(255, 159, 64, 0.6)',
                            'rgba(255, 99, 132, 0.6)'
                        ]
                    }
                ]
            }
        });
    }

    render() {

        if (localStorage.getItem('ROLE') === 'admin') {
            const { back } = this.state;
            if (back) {
                return (<Redirect to={{
                    pathname: '/admin'
                }} />);
            }
        return (

            <div className="App">

                <header className="header-home">
                    <section className="pure-form-section">
                        <form className="pure-form pure-form-mine">
                            <button type="button" id="log-out" className="pure-button pure-button-primary home-buttons" onClick={this.handleBack}>Back</button>
                        </form>
                    </section>
                </header>
                <br/><br/><br/><br/>


                <Bar
                    data={this.state.chartData}
                    options={{
                        title:{
                            display:this.props.displayTitle,
                            text:'Largest Cities In '+this.props.location,
                            fontSize:25
                        },
                        legend:{
                            display:this.props.displayLegend,
                            position:this.props.legendPosition
                        }
                    }}
                />

            </div>
        );
        } else {
            return (
                <Col sm={{offset: 1}}>
                    <br/>
                    <h1> You must login as an Admin! </h1>
                </Col>
            )
        }

    }
}

export default View;
