import {HOST} from '../../commons/host';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    place: '/place',
};



function getPlacesByName(params, callback){
    let request = new Request(HOST.backend_api + endpoint.place + "/sortName/" + params, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function getPlacesByCategory(params, callback){
    let request = new Request(HOST.backend_api + endpoint.place + "/sortCategory/" + params, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


export {
    getPlacesByName,
    getPlacesByCategory,

};