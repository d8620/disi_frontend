import {HOST} from '../../commons/host';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    account: '/person/login',
};

function getAccount(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.account, {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getAccount
}