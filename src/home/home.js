import React from 'react';

import { Redirect } from 'react-router-dom';
import * as API_USERS from './api/home-api.js';
import './home.css';
import * as API_PLACES from "../admin/api/place-api";
import * as API_SEARCH from "../search/api/search-api";
import {number} from "prop-types";
import {Col} from "react-bootstrap";
const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
};
const headerStyle = { backgroundColor: 'white', height: "700px", width: "100%" };

class Home extends React.Component {


    constructor(props) {
        super(props);
        this.emailFilled = "";
        this.passwordFilled= "";
        this.locationFilled="";
        this.categoryFilled="";
        this.exist = true;
        this.state = {
            createAccount: false,
            role: "",
            found: false,
            searchByLocation:false,
            searchByCategory:false,
            email: "",
            password: "",
            placeLocation:"",
            placeCategory:"",
            errors: { email: '', password: '' },
            resetPass: false,
            accountId: "",
            items: [],
            isLoaded: false,
            viewDetailsHome:false

        };

        this.handleCreateAccount = this.handleCreateAccount.bind(this);
        this.handleResetPass = this.handleResetPass.bind(this);
        this.handleLogIn = this.handleLogIn.bind(this);
        this.handleSearchByLocation = this.handleSearchByLocation.bind(this);
        this.handleSearchByCategory = this.handleSearchByCategory.bind(this);
        this.openFacebook = this.openFacebook.bind(this);
        this.openTwitter = this.openTwitter.bind(this);
        this.openInstagram = this.openInstagram.bind(this);
        this.handleViewDetailsHome = this.handleViewDetailsHome.bind(this);
    }

    openFacebook() {
        const url = 'https://www.facebook.com/';
        window.open(url, '_blank');
    }
    openInstagram() {
        const url = 'https://www.instagram.com/';
        window.open(url, '_blank');
    }
    openTwitter() {
        const url = 'https://twitter.com/?lang=ro';
        window.open(url, '_blank');
    }

    handleCreateAccount() {
        this.setState({
            createAccount: true
        })
    }

    handleResetPass() {
        this.setState({
            resetPass: true
        })
    }

    handleSearchByCategory()
    {
        this.setState({
            searchByCategory: true
        })
    }

    handleViewDetailsHome() {
        this.setState({
            viewDetailsHome: true
        })
    }

    handleLogIn() {
        const { email, password } = this.state;
        let errors = { email: '', password: '' };

        errors.email = '';
        errors.password = '';
        if (!email && !password) {
            errors.email = 'Email is required!';
            errors.password = 'Password is required!';
        }
        else if (!email && password) {
            errors.password = 'Password is required!';

        } else if (!password && email) {
            errors.email = 'email is required!';

        } else if (!this.exist) {
            errors.email = 'email is probably wrong!';
            errors.password = 'Password is probably wrong!';
        }
        let user = {
            email: this.state.email,
            password: this.state.password
        }

        sessionStorage.setItem("Email",this.state.email);
        console.log(user);


        this.setState(() => ({ errors: errors }));
        if (this.emailFilled === "" || this.passwordFilled === "") {
            this.emailEmpty = "one field is empty!"
        }
        API_USERS.getAccount(user, (result, status, err) => {
            console.log(result);

            if(result===null)
            {
                (window.confirm("This account doesn't exist"));
            }
            else {

                this.state.email = result.email;
                this.state.accountId = result.id;
                this.state.role = result.role;
                if (status === 200) {
                    this.ok = 1;

                    if (this.ok === 1) {

                        this.setState(() => ({found: true}));

                    } else {
                        if (this.emailFilled !== "" || this.passwordFilled !== "") this.exist = false;
                        console.log("This account with email " + this.emailFilled + "and password " + this.passwordFilled + " doesn't exist with ");
                    }
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }));
                }
            }
        });
    }
    componentDidMount() {
       this.fetchPlaces()
       // this.fetchPlace();

    }

    fetchPlaces() {

        return API_PLACES.getPlaces((result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    items: result,
                    isLoaded: true
                });
            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    handleSearchByLocation() {
        const placeLocation=this.state.placeLocation;
        console.log(placeLocation);

        return API_SEARCH.getPlacesByName(placeLocation,(result, status, err) => {
            if (result !== null && status === 200) {
                console.log(result);
                this.setState({
                    items: result,
                    isLoaded: true,
                    searchByLocation: true

                });
            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    handleSearchByCategory() {
        const placeCategory=this.state.placeCategory;
        console.log(placeCategory);

        return API_SEARCH.getPlacesByCategory(placeCategory,(result, status, err) => {
            if (result !== null && status === 200) {
                console.log(result);
                this.setState({
                    items: result,
                    isLoaded: true,
                    searchByCategory: true

                });
            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }



    render() {
        if (sessionStorage.getItem('ROLE') !== 'admin' || sessionStorage.getItem('ROLE') !== 'user') {

            const {isLoaded, items} = this.state;
            const {createAccount} = this.state;
            const {accountId, email} = this.state
            const {viewDetailsHome} = this.state;

            if (createAccount === true) {
                return (<Redirect to={{
                    pathname: '/create-account'
                }}/>);
            }


            const {resetPass} = this.state;
            if (resetPass === true) {
                return (<Redirect to={{
                    pathname: '/reset-password'
                }}/>)
            }


            if (viewDetailsHome === true) {
                return (<Redirect to={{
                    pathname: '/details'
                }}/>);
            }

            const {found} = this.state;
            const {role} = this.state;
            if (found === true) {
                switch (role) {
                    case "admin":
                        localStorage.setItem('adminId', accountId);
                        localStorage.setItem('ROLE', role);
                        return (<Redirect to={{
                            pathname: "/admin",

                        }}/>);
                    case "user":
                        localStorage.setItem('userId', accountId);
                        localStorage.setItem('ROLE', role);
                        sessionStorage.setItem('email', email);
                        return (<Redirect to={{
                            pathname: "/user",

                        }}/>);

                    default:
                        return <Redirect to='/user'/>;
                }
            }


            return (
                <div>
                    <header className="header-home">
                        <section className="pure-form-section">
                            <form className="pure-form pure-form-mine">
                                <fieldset className="fields-form">
                                    <input type="email" placeholder="Email"
                                           onChange={(e) => {
                                               this.state.email = (e.target.value);
                                               this.emailFilled = (e.target.value);
                                           }}
                                    />
                                    <input type="password" placeholder="Password"
                                           onChange={(e) => {
                                               this.state.password = (e.target.value);
                                               this.passwordFilled = (e.target.value);
                                           }}
                                    />
                                    <button type="button" className="pure-button pure-button-primary home-buttons"
                                            onClick={this.handleLogIn}>Sign in
                                    </button>
                                    <button type="button" className="pure-button pure-button-primary home-buttons"
                                            onClick={this.handleCreateAccount}>Sign up
                                    </button>
                                    <button type="button" className="pure-button pure-button-primary home-buttons"
                                            onClick={this.handleResetPass}>Reset password
                                    </button>

                                </fieldset>
                            </form>
                        </section>
                    </header>
                    <fieldset className="search-field-name">
                        <input type="place location" placeholder="Place location"
                               onChange={(e) => {
                                   this.state.placeLocation = (e.target.value);
                                   this.locationFilled = (e.target.value);
                               }}
                        />
                    </fieldset>
                    <button type="button" className="search-button-name" onClick={this.handleSearchByLocation}>Search by
                        location
                    </button>

                    <fieldset className="search-field-category">
                        <input type="place category" placeholder="Place category"
                               onChange={(e) => {
                                   this.state.placeCategory = (e.target.value);
                                   this.categoryFilled = (e.target.value);

                               }}

                        />
                    </fieldset>
                    <button type="button" className="search-button-category"
                            onClick={this.handleSearchByCategory}>Search by category
                    </button>


                    <main className="main-home">
                        <ul>
                            <br/><br/><br/><br/><br/><br/> <br/> <br/>
                            {items.map(item => (
                                <li key={item.id} className="box">
                                    <div className="column">
                                        <img src={item.image} width="100" height="160">
                                        </img>
                                        {sessionStorage.setItem('Image', item.image)}
                                        <button type="button" className="buttonDetails"
                                                onClick={this.handleViewDetailsHome}>View details
                                        </button>
                                    </div>
                                    <strong> Name: </strong> {item.name} {sessionStorage.setItem('Name', item.name)}
                                    <br/>
                                    <strong> Location: </strong> {item.location} {sessionStorage.setItem('Location', item.location)}
                                    <br/>
                                    <strong> Description: </strong> {item.descriptionText} {sessionStorage.setItem('DescriptionText', item.descriptionText)}
                                    <br/>
                                    <strong> Price: </strong> {item.price} € {sessionStorage.setItem('Price', item.price)}
                                    <br/>
                                    <strong> Category: </strong> {item.category} {sessionStorage.setItem('Category', item.category)}

                                    {sessionStorage.setItem('Audio', item.descriptionAudio)}


                                </li>
                            ))}
                        </ul>

                    </main>
                    <footer>

                        <br/>
                        <center>

                            <div className="icons">
                                <ul className="icons-ul">
                                    <li>
                                        <button id="facebook" onClick={this.openFacebook}></button>
                                    </li>
                                    <li>
                                        <button id="instagram" onClick={this.openInstagram}></button>
                                    </li>
                                    <li>
                                        <button id="twitter" onClick={this.openTwitter}></button>
                                    </li>
                                </ul>
                            </div>
                        </center>

                    </footer>
                </div>

            )
        } else {
            return (
                <Col sm={{offset: 1}}>
                    <br/>
                    <h1> You must login first! </h1>
                </Col>
            )
        }
    }
}

export default Home
