import {HOST} from '../../commons/host';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    place: '/place',
};

function getPlaces(callback) {
    let request = new Request(HOST.backend_api + endpoint.place ,{
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPlaceById(placeId, callback){
    let request = new Request(HOST.backend_api + endpoint.place + "/" + placeId, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function deletePlaceById(placeId,  callback) {
    // if(window.confirm('Are you sure?')) {
        let request = new Request(HOST.backend_api + endpoint.place + "/" + placeId, {
            method: 'DELETE',

        });

        console.log("URL: " + request.url);
        RestApiClient.performRequest(request, callback);
    // }
}

function insertPlace(params, callback){
    let request = new Request(HOST.backend_api + endpoint.place , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(params)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updatePlace(params, callback){
    let request = new Request(HOST.backend_api + endpoint.place , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(params)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function putPlace(id, place, callback) {
    let request = new Request(HOST.backend_api + endpoint.place+ '/' + id, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(place)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}


export {
    getPlaces,
    getPlaceById,
    deletePlaceById,
    updatePlace,
    insertPlace,
    putPlace
};