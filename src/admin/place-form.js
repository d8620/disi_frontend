import React from 'react'
import Button from "react-bootstrap/Button";
import * as API_USERS from './api/place-api';
import {Col, Row} from "reactstrap";

import { FormGroup, Input, Label} from 'reactstrap';

class PlaceForm extends React.Component {
    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        this.selectedData = this.props.selectedData;

        this.state = {
            errorStatus: 0,
            error: null,
            formIsValid: this.props.action === 'update',
            formControls: {
                name: {
                    value: null,
                    placeholder: 'Touristic point name',
                    valid: this.props.action === 'update',
                    touched: false,
                },
                location: {
                    value: null,
                    placeholder: 'Touristic point location',
                    valid: this.props.action === 'update',
                    touched: false,
                },
                nr_of_tourists: {
                    value: null,
                    placeholder: 'Visitors',
                    valid: this.props.action === 'update',
                    touched: false,
                },
                descriptionText: {
                    value: null,
                    placeholder: 'Touristic point description',
                    valid: this.props.action === 'update',
                    touched: false,
                },
                descriptionAudio: {
                    value: null,
                    placeholder: 'Touristic point description',
                    valid: this.props.action === 'update',
                    touched: false,
                },
                price: {
                    value: null,
                    placeholder: 'Touristic point price',
                    valid: this.props.action === 'update',
                    touched: false,
                },
                category: {
                    value: null,
                    placeholder: 'Touristic point category',
                    valid: this.props.action === 'update',
                    touched: false,
                },
                image: {
                    value: null,
                    placeholder: 'Image URL',
                    valid: this.props.action === 'update',
                    touched: false,
                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    selectedData() {
        this.state.formControls.name.value = this.selectedData.name;
        this.state.formControls.location.value = this.selectedData.location;
        this.state.formControls.descriptionText.value = this.selectedData.descriptionText;
        this.state.formControls.price.value = this.selectedData.price;
        this.state.formControls.category.value = this.selectedData.category;
        this.state.formControls.image.value = this.selectedData.image;
        this.state.formControls.nr_of_tourists.value = this.selectedData.nr_of_tourists;
        this.state.formControls.descriptionAudio.value = this.selectedData.descriptionAudio;
    }

    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;
        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    insertPlace(place) {
        console.log(place);
        return API_USERS.insertPlace(place, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted place with id: " + result);
                this.reloadHandler();
            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    updatePlace(place, id) {
        return API_USERS.putPlace(id, place, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated place with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let place = {
            name: this.state.formControls.name.value,
            location: this.state.formControls.location.value,
            descriptionText: this.state.formControls.descriptionText.value,
            price: this.state.formControls.price.value,
            category: this.state.formControls.category.value,
            image: this.state.formControls.image.value,
            nr_of_tourists: this.state.formControls.nr_of_tourists.value,
            descriptionAudio: this.state.formControls.descriptionAudio.value,

        };
        console.log(place);
        this.props.action === 'update' ? this.updatePlace(place, this.props.id) : this.insertPlace(place)
    }

    render() {
        return (
            <div>
                <FormGroup id='category'>
                    <Label for='nameField'> Name: </Label>
                    <Input name = 'name' id = 'nameField' placeholder = {this.state.formControls.name.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.name.value}
                           touched = {this.state.formControls.name.touched? 1 : 0}
                           valid = {this.state.formControls.name.valid}
                           required
                    />
                </FormGroup>


                <FormGroup id = 'category'>
                    <Label for = 'locationField'> Location: </Label>
                    <Input name = 'location' id = 'locationField' placeholder = {this.state.formControls.location.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.location.value}
                           touched = {this.state.formControls.location.touched? 1 : 0}
                           valid = {this.state.formControls.location.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id = 'category'>
                    <Label for = 'nr_of_touristsField'> Number of tourists: </Label>
                    <Input name = 'nr_of_tourists' id = 'nr_of_touristsField' placeholder = {this.state.formControls.nr_of_tourists.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.nr_of_tourists.value}
                           touched = {this.state.formControls.nr_of_tourists.touched? 1 : 0}
                           valid = {this.state.formControls.nr_of_tourists.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='descriptionText'>
                    <Label for='descriptionTextField'> Description: </Label>
                    <Input name = 'descriptionText' id = 'descriptionTextField' placeholder = {this.state.formControls.descriptionText.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.descriptionText.value}
                           touched = {this.state.formControls.descriptionText.touched? 1 : 0}
                           valid = {this.state.formControls.descriptionText.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='price'>
                    <Label for='priceField'> Price: </Label>
                    <Input name = 'price' id = 'priceField' placeholder = {this.state.formControls.price.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.price.value}
                           touched = {this.state.formControls.price.touched? 1 : 0}
                           valid = {this.state.formControls.price.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='category'>
                    <Label for='categoryField'> Category: </Label>
                    <Input name = 'category' id = 'categoryField' placeholder = {this.state.formControls.category.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.category.value}
                           touched = {this.state.formControls.category.touched? 1 : 0}
                           valid = {this.state.formControls.category.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='category'>
                    <Label for='imageField'> Image: </Label>
                    <Input name = 'image' id = 'imageField' placeholder = {this.state.formControls.image.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.image.value}
                           touched = {this.state.formControls.image.touched? 1 : 0}
                           valid = {this.state.formControls.image.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='category'>
                    <Label for='descriptionAudioField'> Audio: </Label>
                    <Input name = 'descriptionAudio' id = 'descriptionAudioField' placeholder = {this.state.formControls.descriptionAudio.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.state.formControls.descriptionAudio.value}
                           touched = {this.state.formControls.descriptionAudio.touched? 1 : 0}
                           valid = {this.state.formControls.descriptionAudio.valid}
                           required
                    />
                </FormGroup>

                <Row>
                    <Col sm = {{size: '4', offset: 5}}>
                        <Button type = {"submit"} onClick={this.handleSubmit}> Submit </Button>
                    </Col>
                </Row>

            </div>
        );
    }
}
export default PlaceForm;