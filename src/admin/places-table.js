import React from "react";
import Table from "../commons/tables/table";
import * as API_USERS from "./api/place-api";
import {Button, Modal, ModalBody, ModalHeader} from "reactstrap";
import PlaceEditForm from "./placeEdit-form";

const filters1 = [
    {
        accessor: 'name'

    },
    {

        accessor: 'category'
    },
    {

        accessor: 'location'
    }
];



class PlacesTable extends React.Component{

    constructor(props) {
        super(props);

        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        this.reload = this.reload.bind(this);


        this.state = {
            selectedData: {},
            selected: false,
            collapseForm: false,
            isChecked: false,
            tableData: this.props.tableData,
            type: "place"
        };
        this.crudClient = [];
        this.columns = [
            {
                Header: 'Name',
                accessor: 'name',
            },
            {
                Header: 'Location',
                accessor: 'location',
            },
            {
                Header: 'Text description',
                accessor: 'descriptionText',
            },
            {
                Header: 'Audio description',
                accessor: 'descriptionAudio',
            },
            {
                Header: 'Price',
                accessor: 'price',
            },
            {
                Header: 'Category',
                accessor: 'category',
            },
            {
                Header: 'Discount',
                accessor: 'discount',
            },
            {
                Header: 'Visitors',
                accessor: 'nr_of_tourists',
            },

            {
                Header: 'Operations',
                accessor: 'operations',
                Cell: (row) => (
                    <div>
                        <Button color="primary" onClick={(e) => this.handleDelete(e, row)}>Delete</Button>
                        <Button color="primary" onClick={() => {
                            this.setState({selectedData: row.original})
                            this.toggleForm()}}>Edit</Button>
                    </div>
                )
            }
        ];
    }

    deletePlace(id) {
        return API_USERS.deletePlaceById(id, (result, status, err) => {
            if (result !== null && (status === 200 || status === 201)) {
                window.alert("Successfully deleted place");
                this.reloadHandler();
            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    handleDelete = (event, row) => {
        if (window.confirm("Are you sure?")) {
            this.crudPlace = [];
            this.crudPlace.push(row.original);
            // console.log(this.crudPlace[0].id);
            this.deletePlace(this.crudPlace[0].id);
        }

    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    reload() {
        this.setState( {
            isLoaded: false
        });
        this.toggleForm();
    }

    render() {
        return (
            <div>
                <main className="admin-home">
                <Table data = {this.state.tableData}
                       columns = {this.columns}
                       search = {filters1}
                       pageSize = {10}
                />

                <Modal isOpen={this.state.selected} toggle={this.toggleForm} className={this.props.className} size="lg">
                       <ModalHeader toggle={this.toggleForm}>    Edit </ModalHeader>
                    <ModalBody>
                        <PlaceEditForm reloadHandler = {this.reload}
                                        selectedData = {this.state.selectedData}
                                        toggleForm = {this.toggleForm}
                        />
                    </ModalBody>
                </Modal>
                    </main>
            </div>
        )
    }
}
export default PlacesTable;