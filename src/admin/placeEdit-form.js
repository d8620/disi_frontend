import React from 'react'
import Button from "react-bootstrap/Button";
import * as API_USERS from './api/place-api';
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';

class PlaceEditForm extends React.Component {
    constructor(props) {
        super(props);
        this.toggleForm = this.props.toggleForm;
        this.reloadHandler = this.props.reloadHandler;

        this.state = {
            errorStatus: 0,
            error: null,
            formIsValid: false,
            formControls: {
                name: {
                    value: '',
                    placeholder: 'Touristic point name',
                    valid: false,
                    touched: false,
                },
                location: {
                    value: '',
                    placeholder: 'Touristic point location',
                    valid: false,
                    touched: false,
                },
                descriptionText: {
                    value: '',
                    placeholder: 'Touristic point description',
                    valid: false,
                    touched: false,
                },
                descriptionAudio: {
                    value: '',
                    placeholder: 'Touristic point description',
                    valid: false,
                    touched: false,
                },
                price: {
                    value: '',
                    placeholder: 'Touristic point description',
                    valid: false,
                    touched: false,
                },
                category: {
                    value: '',
                    placeholder: 'Touristic point category',
                    valid: false,
                    touched: false,
                },
                discount: {
                    value: '',
                    placeholder: 'Price discount',
                    valid: false,
                    touched: false,
                },
                image: {
                    value: '',
                    placeholder: 'Image URL',
                    valid: false,
                    touched: false,
                },
                nr_of_tourists: {
                    value: '',
                    placeholder: 'Visitors',
                    valid: false,
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    selectedData() {
        this.state.formControls.name.value = this.selectedData.name;
        this.state.formControls.location.value = this.selectedData.location;
        this.state.formControls.descriptionText.value = this.selectedData.descriptionText;
        this.state.formControls.price.value = this.selectedData.price;
        this.state.formControls.category.value = this.selectedData.category;
        this.state.formControls.discount.value = this.selectedData.discount;
        this.state.formControls.image.value = this.selectedData.image;
        this.state.formControls.nr_of_tourists.value = this.selectedData.nr_of_tourists;
        this.state.formControls.descriptionAudio.value = this.selectedData.descriptionAudio;
    }

    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;

        this.props.selectedData[name] = value;
    };

    updatePlace(place, id) {

        return API_USERS.putPlace(id, place, (result, status, error) => {
            console.log(place);
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated place with id: " + result);
                //this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let place = {
            name: this.props.selectedData['name'],
            location: this.props.selectedData['location'],
            descriptionText: this.props.selectedData['descriptionText'],
            price: this.props.selectedData['price'],
            category: this.props.selectedData['category'],
            discount: this.props.selectedData['discount'],
            image: this.props.selectedData['image'],
            nr_of_tourists: this.props.selectedData['nr_of_tourists'],
            descriptionAudio: this.props.selectedData['descriptionAudio'],

        };
        console.log(this.props.selectedData['id']);
        this.updatePlace(place, this.props.selectedData['id']);
    }

    render() {
        return (
            <div>
                <main className="admin-edit">

                <FormGroup id='category'>
                  <b>  <Label for='nameField'> Name: </Label></b>
                    <Input name = 'name' id = 'nameField' placeholder = {this.state.formControls.name.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.props.selectedData['name']}
                           required
                    />
                </FormGroup>


                <FormGroup id = 'category'>
                   <b><Label for = 'locationField'> Location: </Label></b>
                    <Input name = 'location' id = 'locationField' placeholder = {this.state.formControls.location.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.props.selectedData['location']}
                           required
                    />
                </FormGroup>


                <FormGroup id='descriptionText'>
                    <b> <Label for='descriptionTextField'> Description: </Label></b>
                    <Input name = 'descriptionText' id = 'descriptionTextField' placeholder = {this.state.formControls.descriptionText.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.props.selectedData['descriptionText']}
                           required
                    />
                </FormGroup>

                <FormGroup id='price'>
                    <b> <Label for='priceField'> Price: </Label></b>
                <Input name = 'price' id = 'priceField' placeholder = {this.state.formControls.price.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.props.selectedData['price']}
                           required
                    />
                </FormGroup>

                <FormGroup id='category'>
                    <b> <Label for='categoryField'> Category: </Label></b>
        <Input name = 'category' id = 'categoryField' placeholder = {this.state.formControls.category.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.props.selectedData['category']}
                           required
                    />
                </FormGroup>

                <FormGroup id='category'>
                   <b> <Label for='discountField'> Discount: </Label></b>
        <Input name = 'discount' id = 'discountField' placeholder = {this.state.formControls.discount.placeholder}
                           onChange = {this.handleChange}
                           defaultValue = {this.props.selectedData['discount']}
                           required
                    />
                </FormGroup>

                    <FormGroup id='category'>
                        <b> <Label for='nr_of_touristsField'> Visitors: </Label></b>
                        <Input name = 'nr_of_tourists' id = 'nr_of_touristsField' placeholder = {this.state.formControls.nr_of_tourists.placeholder}
                               onChange = {this.handleChange}
                               defaultValue = {this.props.selectedData['nr_of_tourists']}
                               required
                        />
                    </FormGroup>

                    <FormGroup id = 'category'>
                        <b><Label for = 'imageField'> Image: </Label></b>
                        <Input name = 'image' id = 'imageField' placeholder = {this.state.formControls.image.placeholder}
                               onChange = {this.handleChange}
                               defaultValue = {this.props.selectedData['image']}
                               required
                        />
                    </FormGroup>

                    <FormGroup id='category'>
                        <b> <Label for='descriptionAudioField'> Audio: </Label></b>
                        <Input name = 'descriptionAudio' id = 'descriptionAudioField' placeholder = {this.state.formControls.descriptionAudio.placeholder}
                               onChange = {this.handleChange}
                               defaultValue = {this.props.selectedData['descriptionAudio']}
                               required
                        />
                    </FormGroup>


                </main>
                 <br/>
                <Row>
                    <Col sm = {{size: '4', offset: 5}}>
                        <Button type = {"submit"} onClick={() => {
                            this.handleSubmit()
                            this.toggleForm()
                        }}> Submit </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default PlaceEditForm;