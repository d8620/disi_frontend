import * as API_USERS from "./api/place-api"
import React from "react";
import {ModalHeader, Card, Row, Button, Col, Modal, ModalBody} from "reactstrap";
import PlaceForm from "./place-form";
import PlacesTable from "./places-table";
import {Redirect} from "react-router-dom";

class AdminPage extends React.Component {
    constructor(props) {
        super(props);
        this.handleLogOut = this.handleLogOut.bind(this);
        this.handleChartObjective= this.handleChartObjective.bind(this);
        this.handleChartLocation= this.handleChartLocation.bind(this);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.handleSelectRow = this.handleSelectRow.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
        this.reloadHand = this.reloadHand.bind(this);
        this.state = {
            selected: false,
            collapseForm: false,
            redirectLogout: false,
            redirectChartObjective: false,
            redirectChartLocation: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            selectedRow: false,
            action: null,
            id: null,
            place: {
                name: null,
                location: null,
                descriptionText: null,
                price: null,
                category: null
            }
        };
    }


    componentDidMount() {
        this.fetchPlaces();
    }

    fetchPlaces() {
        return API_USERS.getPlaces((result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });

            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });


    }


    handleUpdate() {
        this.setState({action: 'update'});
        this.toggleForm()
    }

    handleAdd() {
        this.setState({action: 'add'});
        this.toggleForm()
    }

    handleSelectRow(id) {
        this.setState({
            selectedRow: true,
            id: id.id,
            place: {
                name: id.name,
                location: id.location,
                descriptionText: id.descriptionText,
                price: id.price,
                category: id.category
            }
        })
    }

    reloadHand() {
        this.setState({
            isLoaded: false
        });
        this.fetchPlaces();
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    reload() {
        this.setState( {isLoaded: false});
        this.toggleForm();
        this.fetchPlaces();
    }

    handleLogOut() {

        this.setState(({
            redirectLogout: true
        }));
    }

    handleChartObjective() {

        this.setState(({
            redirectChartObjective: true
        }));
    }


    handleChartLocation() {

        this.setState(({
            redirectChartLocation: true
        }));
    }

    render() {
        if(localStorage.getItem('ROLE') === 'admin') {

        const { redirectLogout } = this.state;
            const { redirectChartObjective} = this.state;
            const { redirectChartLocation} = this.state;
        if (redirectLogout === true) {

            return (<Redirect to={{
                pathname: '/'
            }} />);
        }
            if (redirectChartObjective === true) {

                return (<Redirect to={{
                    pathname: '/objectivesGraphic'
                }} />);
            }

            if (redirectChartLocation === true) {

                return (<Redirect to={{
                    pathname: '/locationsGraphic'
                }} />);
            }
            return (
                <div>
                    <header className="header-home">
                        <section className="pure-form-section">
                            <form className="pure-form pure-form-mine">

                                <button type="button" id="log-out" className="pure-button pure-button-primary" onClick={this.handleLogOut}>Log out</button>

                            </form>
                        </section>
                    </header>

                    <div style={{backgroundColor: "white"}}>
                        <Col sm={{offset: 1}}>
                            <br/><br/><br/><center>
                            <h1> Hello Admin! </h1></center>
                        </Col>
                    </div>
                    <Card>
                        <br/>
                        <Row>

                            <Col sm={{size: '8', offset: 1}}>
                                <Button color="primary" size={"lg"} onClick={this.handleChartObjective}>View the most visited touristic objective</Button>
                                <br/>
                            </Col>
                            <Col sm={{size: '8', offset: 1}}>
                                <Button color="primary" size={"lg"} onClick={this.handleChartLocation}>View the most visited touristic locations</Button>
                                <br/>
                            </Col>
                            <br/>
                            <Col sm={{size: '8', offset: 1}}>
                                <Button color="primary" size={"lg"} onClick={this.handleAdd}>Add touristic point</Button>
                            </Col>

                        </Row>
                        <br/>
                        <Row>
                            <Col sm={{size: '7', offset: 1}}>
                            <b>   {this.state.isLoaded && <PlacesTable tableData = {this.state.tableData}
                                                                      reloadHandler = {this.reloadHand}
                                                                      handleSelectRow = {this.handleSelectRow.bind(this)} />}</b>
                            </Col>
                        </Row>
                    </Card>

                    <Modal isOpen={this.state.selected} toggle={this.toggleForm} className={this.props.className} size="lg">
                        <ModalHeader toggle={this.toggleForm}>Add touristic point</ModalHeader>
                        <ModalBody>
                            <PlaceForm reloadHandler = {this.reload}
                            />
                        </ModalBody>
                    </Modal>


                    <br/><br/>  <br/><br/>  <br/><br/>  <br/><br/>  <br/><br/>


                    <footer >
                           <center>
                               <br/>
                        <div className="icons">
                            <ul className="icons-ul">
                                <li><button id="facebook" onClick={this.openFacebook}></button></li>
                                <li><button id="instagram" onClick={this.openInstagram}></button></li>
                                <li><button id="twitter" onClick={this.openTwitter}></button></li>
                            </ul>
                        </div>
                           </center>

                    </footer>
                </div>
            );
        }
        else{
                return (
                    <Col sm={{offset: 1}}>
                        <br/>
                        <h1> You must login as an Admin! </h1>
                    </Col>
                )
            }
        }
}
export default AdminPage;