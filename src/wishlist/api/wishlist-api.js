import {HOST} from '../../commons/host';
import RestApiClient from "../../commons/api/rest-client";



const endpoint = {
    comment: '/user',
};

function getWishlist(userId, callback) {
    let request = new Request(HOST.backend_api + endpoint.comment+"/"+ userId,{
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}



export {
    getWishlist

};