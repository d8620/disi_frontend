import React from 'react';
import { Redirect } from 'react-router-dom';

import * as API_WISHLIST from "../wishlist/api/wishlist-api";
import {Col} from "react-bootstrap";

class Wishlist extends React.Component {

    constructor(props) {
        super(props);
        this.id = null;
        this.handleBack = this.handleBack.bind(this);
        this.state = {
            back: false,
            items: [],
            isLoaded: false,
        };


    }
    openFacebook() {
        const url = 'https://www.facebook.com/';
        window.open(url, '_blank');
    }
    openInstagram() {
        const url = 'https://www.instagram.com/';
        window.open(url, '_blank');
    }
    openTwitter() {
        const url = 'https://twitter.com/?lang=ro';
        window.open(url, '_blank');
    }

    componentDidMount() {
        this.fetchPlace(this.id);
    }

   // id=localStorage.getItem("userID");

    fetchPlace() {
        const id=localStorage.getItem("userId");
        console.log(id);
        return API_WISHLIST.getWishlist(id, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    items: result,
                    isLoaded: true
                });
                console.log(id+" "+result);
            }
            else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });

    }

    handleBack() {
        this.setState(({
            back: true
        }))
    }
    render() {
        if (localStorage.getItem('ROLE') === 'user') {

        const { back } = this.state;
        const { items} = this.state;
        if (back) {
            return (<Redirect to={{
                pathname: '/user'
            }} />);
        }
        return (
            <div>
                <header className="header-home">
                    <section className="pure-form-section">
                        <form className="pure-form pure-form-mine">
                            <button type="button" id="log-out" className="pure-button pure-button-primary home-buttons" onClick={this.handleBack}>Back</button>
                        </form>
                    </section>
                </header>

                <main className="user-home">

                    <ul>
                        <br/><br/><br/><br/><br/>
                        <div className="columnWishlist">
                        <h1>WISHLIST</h1>
                        </div>
                        <br/><br/><br/>
                        {items.map(item => (
                            <li key={item.id} className="box">
                                {localStorage.setItem('id_place', item.id)}
                                <div className="column">
                                    <img src={item.image} width="100" height="160">
                                    </img>
                                    {sessionStorage.setItem('Image', item.image)}

                                </div>
                                <strong> Name: </strong> {item.name} {sessionStorage.setItem('Name', item.name)}
                                <br/>
                                <strong> Location: </strong> {item.location} {sessionStorage.setItem('Location', item.location)}
                                <br/>
                                <strong> Description: </strong> {item.descriptionText} {sessionStorage.setItem('DescriptionText', item.descriptionText)}
                                <br/>
                                <strong> Price: </strong> {item.price} € {sessionStorage.setItem('Price', item.price)}
                                <br/>
                                <strong> Category: </strong> {item.category} {sessionStorage.setItem('Category', item.category)}

                            </li>
                        ))}
                    </ul>


                </main>

            </div>
        )
        } else {
            return (
                <Col sm={{offset: 1}}>
                    <br/>
                    <h1> You must login as an Tourist! </h1>
                </Col>
            )
        }
    }


}
export default Wishlist;

